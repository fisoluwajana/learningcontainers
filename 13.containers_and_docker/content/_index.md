+++
title = "Containers and Docker"
outputs = ["Reveal"]

[reveal_hugo]
theme = "league"
transition= 'concave'
controls= true
progress= true
history= true
center= true


+++

# Containers 

---

## Compute - The State of Play

- What do we remember about compute? 
{{% note %}} Think key words - EC2, VM, ELB, scalable, IaaS etc. Ask about benefits! {{% /note %}}

---

## Compute - The State of Play

- Virtual servers are provisioned in replacement for physical racks 
- Flexible, highly scalable 
- Pay based on consumption rather than in advance 
- e.g. Amazon EC2, Azure, Google VMs
- N.B We've only really discussed IaaS so far!

---

<img src="https://blogs.bmc.com/wp-content/uploads/2017/09/iaas-paas-saas-comparison.jpg" width="800" height="600" />

---

## What are we trying to solve? 

- Software may not run reliable when ported between different environments 
- e.g Developers laptop to a test environment, staging environment to production or a physical server in a data centre to a VM 
- For example if Python 2.7 is running in the data centre while Python 3 is running on your VM there may be weird compatibility issues

--- 

## Virtualization

{{% note %}} Does anybody remember anything about this? {{% /note %}}

---

## Virtualization

- Allows you to use a physical machine’s full capacity by distributing its capabilities among many environments
- Multiple OS's can be running independently on a single machine
- The *hypservisor* is used run guest OS's and emulate standard resources such as CPU, memory and hard drive that can be shared to multiple VMs
- If an application *and* it's environment can be virtualised we can avoid compatibility issues 

--- 

## Containers 

- Containerisation is an extension of the virtualisation approach
- Hypervisor is eliminated - instead virtual containers contain the application and everything that it needs to run (runtime, libraries etc.)
- No virtualised hardware - all containers share the same resources and run on "bare metal"
- Much more lightweight than virtual machines - can be started up much faster, many more containers can be hosted and are very easy to share 

---

## Containers

![](https://s16315.pcdn.co/wp-content/uploads/2014/07/docker_vm.jpg)

{{% note %}} Called containers compared to the modern shipping industry as it only works so well due to the sizes of shipping containers being standardised. Before the advent of this standard, shipping anything in bulk was a complicated, laborious process. {{% /note %}}

---

## Docker 

- Containers have been around for many years - Google have used their own container technology and there are many other container technologies such as Solaris and BSD Jails 
- Docker is a tool designed to create, deploy and run applications using containers. Docker popularised the user of containers 
- Docker allows for ease of use, speed, and scalability 
- Originally built for Linux, it can now also run on Windows and Mac

---

## Docker Components 

- Dockerfile 
- Docker Image 
- Image Registry
- Docker Run 
- Docker Client
- Docker Engine/Daemon

---

## Dockerfile 
- Easy to understand text file with instructions to build a Docker image
- Specifies underlying OS, runtimes, libraries, environments variables, file locations network ports and the actual function it will be performing

```
FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python ./index.py
```

---

## Docker Image 
- Invoke `docker build` to create an image based on a Dockerfile
- The image is a set of instructions containing what a resulting container will run and how 
- Once created a docker file is static and cannot be changed 

---

## Docker Registry 
- Storage for Docker images - imagine GitHub/BitBucket for Docker images 
- Well established registries - DockerHub, GCR, ECS etc. 
- Can host your own private image registry
- Image repositories contain many images with the same name but different tags (e.g version number, latest, stable, alpine)
- No need to re-invent the wheel - public registries contain many images that can be re-used instead of creating your own 

---

## Docker Run
- A `docker run ...` command actually launches a container, each of which is an instance of an image 
- Any running container can be stopped and restarted in the same state (with `docker start`)
- Multiple container instances of the same image can be run as long as they have unique name 

---

## Docker Client 

- Also known as the CLI, this is the primary way users interact with Docker 
- Commands such as `build`, `run` or `pull` are sent by the client to the daemon which carries them out 

## Docker Daemon 

- A service that runs on your host operating system and is exposed to your client as a REST API 
- Listens for API requests and manages objects such as images, containers and volumes

---

## Architecture 

![](https://docs.docker.com/engine/images/architecture.svg)

---

## Benefits of Containers and Docker

- *Easy to use* - Anybody can use Docker to easily build, run and test applications on their local machines, public, private cloud or even physical servers
- *Speed* - faster to run containers as they are more lightweight than VMs and take up less resources
- *Public Registries* - DockerHub and others provide multiple public images ready for use with little/no modification
- *Scalability* - it's easy to adopt a microservices architecture, split parts into separate containers and link them together

--- 

## Downsides?

- *Platform dependent* - Linux is supported but a VM is still used to run Docker on Windows and MacOS X
- *Persistent Data* - Contents of a container are deleted when the container shuts down unless saved elsewhere. Data Volumes intend to solve this but it is still complicated
- *Linking Containers* - Spinning up a multi-tier architecture with containers across different machines can be complicated without the use of tools such as Docker Compose, Docker Swarm or Kubernetes 

---

## Docker & the Cloud 

Cloud providers offer more than just image registries. While it's possible to host Docker on a VM running in the cloud, there are many services created to make running Docker in the cloud easier:
- Amazon Elastic Container Service (ECS), Fargate and Elastic Kubernetes Service (EKS)
- Azure Container Service/Kubernetes Service (AKS) 
- Google Compute Engine (GCE) and Google Kubernetes Engine (GKE)


---

## Docker & the Cloud 

There are also managed PaaS options that use Docker and containers in their underlying infrastructure. These include:
- Amazon Elastic Beanstalk 
- Google App Engine. 

## Further/Useful Reads
[Docker - What is a Container?](https://www.docker.com/resources/what-container)
[Virtualisation is Dead](https://diginomica.com/virtualization-dead-long-live-containerization/)
[Containerise a Python App in 5 minutes](https://www.wintellect.com/containerize-python-app-5-minutes/)
[PaaS Under the Hood](https://web.archive.org/web/20141217113935/http://blog.dotcloud.com/under-the-hood-linux-kernels-on-dotcloud-part)
[History of Containers](https://searchitoperations.techtarget.com/feature/Dive-into-the-decades-long-history-of-container-technology)


